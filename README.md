# heyo! 👋

## whoami

- samr (prounounced "summer" ☀️)
- computer person 💻
- very cool 😎
- light theme person 💡
- fun fact: [object Object]

## projects

- [somestrangegame](https://github.com/samrpf/somestrangegame)
- [who is whom?](https://github.com/samrpf/whoiswhom)
- [smvs](https://github.com/samrpf/smvs)
- [unwritten-tasks](https://unwritten-tasks.samrpf.repl.co/)

## where

### websites

- [wordpress website](https://samrland.wordpress.com)
- [from-scratch website v1](https://samrpf.repl.co)
- [from-scratch website v2](https://samrpf-wip.samrpf.repl.co)

### codings

- [github (most work)](https://github.com/samrland)
- [codeberg (some work)](https://codeberg.org/samrland)
- [gitlab (no work)](https://gitlab.com/samrland)
